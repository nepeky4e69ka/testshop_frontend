export type Profile = {
  _id?: string;
  email: string;
};
export type State = {
  isSignin: boolean;
  isAuthenticated: boolean;
  loading: boolean;
  error: string | null;
  profile: Profile | null;
  [key: string]: unknown;
  errors: { [key: string]: boolean };
  loadings: { [key: string]: boolean };
};

export const state: State = {
  isSignin: true,
  isAuthenticated: false,
  loading: false,
  profile: null,
  loadings: {},
  errors: {},
  error: null,
};

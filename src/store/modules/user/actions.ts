import { ActionTree, ActionContext } from "vuex";
import { State as RootState } from "@/store";
import { State } from "./state";
import { Mutations, MutationTypes } from "./mutations";
import {
  axios,
  AxiosResponse,
  createHttp,
  destroyHttp,
  ErrorHandler,
  AxiosError,
} from "@/utils/axios";
import { ApiRoutes } from "@/utils/api";
import { removeCookie, setCookie } from "@/utils/helper";
const { LOGOUT, SET_USER_AUTHENTICATED } = MutationTypes;

// Action enums
export enum ActionTypes {
  LOGIN = "LOGIN",
  LOGOUT = "LOGOUT",
}
export enum UserActionTypes {
  LOGIN = "user/LOGIN",
  LOGOUT = "user/LOGOUT",
}
interface AxiosUserLoginResponse extends AxiosResponse {
  data: UserLoginResponse;
}

export type UserLoginResponse = {
  token: string;
};
export type UserLoginPayload = {
  email: string;
  password: string;
};
type ArgumentActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload?: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, RootState>, "commit">;

type ctx = ArgumentActionContext;

// Actions contracts
export interface Actions {
  [ActionTypes.LOGOUT]({ commit }: ctx): void;
  [ActionTypes.LOGIN](
    { commit }: ctx,
    payload: UserLoginPayload
  ): Promise<boolean>;
}

// Define actions
export const actions: ActionTree<State, RootState> & Actions = {
  async [ActionTypes.LOGIN](
    { commit, state },
    { email, password }: UserLoginPayload
  ): Promise<boolean> {
    try {
      if (!email || !password) {
        return false;
      }
      const authUrl = state.isSignin
        ? ApiRoutes.USER_SIGNIN
        : ApiRoutes.USER_SIGNUP;
      const response: AxiosUserLoginResponse = await axios.post(authUrl, {
        email,
        password,
      });
      if (!response || !response.data) return false;
      const { token } = response.data;
      commit(SET_USER_AUTHENTICATED, { token });
      createHttp(token);
      setCookie(token, 72);
    } catch (error) {
      ErrorHandler(error as AxiosError);
    }
    return state.isAuthenticated;
  },
  [ActionTypes.LOGOUT]({ commit }) {
    try {
      commit(LOGOUT);
      removeCookie();
      destroyHttp();
    } catch (error) {
      console.log(error);
    }
  },
};

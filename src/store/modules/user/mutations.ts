import { MutationTree } from "vuex";
import { State } from "./state";
import { SetValuePayload, SetSubValuePayload } from "@/store/types";
import { UserLoginResponse } from "./actions";
import { createHttp, destroyHttp } from "@/utils/axios";
import { setCookie, removeCookie, jwtDecodeToUser } from "@/utils/helper";

// mutations enums
export enum MutationTypes {
  SET_VALUE = "SET_VALUE",
  SET_SUB_VALUE = "SET_SUB_VALUE",
  SET_USER_AUTHENTICATED = "SET_USER_AUTHENTICATED",
  LOGOUT = "LOGOUT",
}
export enum UserMutationTypes {
  SET_VALUE = "user/SET_VALUE",
  SET_SUB_VALUE = "user/SET_SUB_VALUE",
  SET_USER_AUTHENTICATED = "user/SET_USER_AUTHENTICATED",
  LOGOUT = "user/LOGOUT",
}
export type UserAuthPayload = {
  email: string;
  password: string;
};
// Mutation contracts
export type Mutations<S = State> = {
  [MutationTypes.SET_VALUE](state: S, payload: SetValuePayload): void;
  [MutationTypes.SET_SUB_VALUE](state: S, payload: SetSubValuePayload): void;
  [MutationTypes.SET_USER_AUTHENTICATED](
    state: S,
    payload: UserLoginResponse
  ): void;
  [MutationTypes.LOGOUT](state: S): void;
};

// Define mutations
export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.SET_VALUE](state: State, payload: SetValuePayload) {
    const { key, value } = payload;
    state[key] = value;
  },
  [MutationTypes.SET_SUB_VALUE](state: State, payload: SetSubValuePayload) {
    const { key, value, sub } = payload;
    try {
      if (!state[key]) state[key] = {};
      state[key] = Object.assign(state[key], { [sub]: value });
    } catch (e) {
      console.error(MutationTypes.SET_SUB_VALUE, { payload }, e);
      void e;
    }
  },
  [MutationTypes.SET_USER_AUTHENTICATED](
    state: State,
    { token }: UserLoginResponse
  ) {
    state.loading = false;
    if (token) {
      state.profile = jwtDecodeToUser(token);
      state.isAuthenticated = true;
      setCookie(token, 72);
      createHttp(token);
    }
  },
  [MutationTypes.LOGOUT](state: State) {
    state.isAuthenticated = false;
    removeCookie();
    destroyHttp();
  },
};

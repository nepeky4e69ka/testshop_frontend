import { Product } from "../../types";

export type State = {
  product: Product;
  products: Product[];
  isMoreAvaliable: boolean;
  limit: number;
  skip: number;
  sort: {
    created: number;
    price: number | null;
  };
  [key: string]: unknown;
};

export const state: State = {
  products: [],
  product: {
    _id: undefined,
    name: "New product",
    price: 100,
    medias: [],
  },
  isMoreAvaliable: false,
  limit: 5,
  skip: 0,
  sort: {
    created: -1,
    price: 1,
  },
};

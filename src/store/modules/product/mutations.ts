import { MutationTree } from "vuex";
import { State } from "./state";
import {
  SetValuePayload,
  SetSubValuePayload,
  Product,
  Media,
} from "@/store/types";

// mutations enums
export enum MutationTypes {
  SET_VALUE = "SET_VALUE",
  SET_SUB_VALUE = "SET_SUB_VALUE",
  ADD_PRODUCT = "ADD_PRODUCT",
  UPDATE_PRODUCT = "UPDATE_PRODUCT",
  DELETE_PRODUCT = "DELETE_PRODUCT",
  SET_DEFAULT_PRODUCT = "SET_DEFAULT_PRODUCT",
  SET_PRODUCT_MEDIAS = "SET_PRODUCT_MEDIAS",
  TOGGLE_SORT = "TOGGLE_SORT",
}
export enum ProductMutationTypes {
  SET_VALUE = "product/SET_VALUE",
  SET_SUB_VALUE = "product/SET_SUB_VALUE",
  SET_DEFAULT_PRODUCT = "product/SET_DEFAULT_PRODUCT",
  SET_PRODUCT_MEDIAS = "product/SET_PRODUCT_MEDIAS",
  TOGGLE_SORT = "product/TOGGLE_SORT",
}
enum Sort {
  price = "price",
  created = "created",
}
// Mutation contracts
export type Mutations<S = State> = {
  [MutationTypes.SET_VALUE](state: S, payload: SetValuePayload): void;
  [MutationTypes.SET_SUB_VALUE](state: S, payload: SetSubValuePayload): void;
  [MutationTypes.ADD_PRODUCT](state: S, product: Product): void;
  [MutationTypes.UPDATE_PRODUCT](state: S, product: Product): void;
  [MutationTypes.DELETE_PRODUCT](state: S, id: string): void;
  [MutationTypes.SET_DEFAULT_PRODUCT](state: S): void;
  [MutationTypes.SET_PRODUCT_MEDIAS](state: S, payload: Media[]): void;
  [MutationTypes.TOGGLE_SORT](state: S, type: Sort): void;
};

// Define mutations
export const mutations: MutationTree<State> & Mutations = {
  [MutationTypes.SET_VALUE](state: State, payload: SetValuePayload) {
    const { key, value } = payload;
    state[key] = value;
  },
  [MutationTypes.SET_SUB_VALUE](state: State, payload: SetSubValuePayload) {
    const { key, value, sub } = payload;
    try {
      if (!state[key]) state[key] = {};
      state[key as string] = Object.assign(state[key], { [sub]: value });
    } catch (e) {
      console.error(MutationTypes.SET_SUB_VALUE, { payload }, e);
      void e;
    }
  },
  [MutationTypes.ADD_PRODUCT](state: State, product: Product) {
    state.products.unshift(product);
  },
  [MutationTypes.UPDATE_PRODUCT](state: State) {
    const idx = state.products.findIndex(
      (product: Product) => product._id === state.product._id
    );
    if (idx > -1) {
      state.products[idx] = state.product;
    }
  },
  [MutationTypes.DELETE_PRODUCT](state: State, _id: string) {
    const idx = state.products.findIndex(
      (product: Product) => product._id === _id
    );
    if (idx > -1) {
      state.products.splice(idx, 1);
    }
  },
  [MutationTypes.SET_DEFAULT_PRODUCT](state: State) {
    state.product = {
      name: `New Product ${state.products.length}`,
      price: 100,
      medias: [],
    };
  },
  [MutationTypes.SET_PRODUCT_MEDIAS](state: State, medias) {
    state.product.medias = medias;
  },
  [MutationTypes.TOGGLE_SORT](state: State, type) {
    state.sort[type] = state.sort[type] === -1 ? 1 : -1;
  },
};

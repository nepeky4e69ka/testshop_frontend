import { ActionTree, ActionContext } from "vuex";
import { State as RootState } from "@/store";
import { State } from "./state";
import { Mutations, MutationTypes } from "./mutations";
import { axios } from "@/utils/axios";
import { ApiRoutes } from "@/utils/api";
import { Media, ProductResponse } from "../../types";
import { uploadHeader } from "@/utils/helper";

// Action enums
export enum ActionTypes {
  GET_PRODUCTS = "GET_PRODUCTS",
  CREATE_PRODUCT = "CREATE_PRODUCT",
  UPDATE_PRODUCT = "UPDATE_PRODUCT",
  DELETE_PRODUCT = "DELETE_PRODUCT",
  UPLOAD_MEDIAS = "UPLOAD_MEDIAS",
}
export enum ProductActionTypes {
  GET_PRODUCTS = "product/GET_PRODUCTS",
  CREATE_PRODUCT = "product/CREATE_PRODUCT",
  UPDATE_PRODUCT = "product/UPDATE_PRODUCT",
  DELETE_PRODUCT = "product/DELETE_PRODUCT",
  UPLOAD_MEDIAS = "product/UPLOAD_MEDIAS",
}

type ArgumentActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload?: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<State, RootState>, "commit">;

type ctx = ArgumentActionContext;

// Actions contracts
export interface Actions {
  [ActionTypes.GET_PRODUCTS]({ commit }: ctx, isSort?: boolean): void;
  [ActionTypes.CREATE_PRODUCT]({ commit }: ctx): void;
  [ActionTypes.UPDATE_PRODUCT]({ commit }: ctx): void;
  [ActionTypes.DELETE_PRODUCT]({ commit }: ctx, id: string): void;
  [ActionTypes.UPLOAD_MEDIAS](
    { commit }: ctx,
    media: unknown
  ): Promise<Media[]>;
}

// Define actions
export const actions: ActionTree<State, RootState> & Actions = {
  async [ActionTypes.GET_PRODUCTS]({ commit, state }, isSort: boolean) {
    try {
      if (isSort) {
        commit(MutationTypes.SET_VALUE, {
          key: "skip",
          value: 0,
        });
      } else {
        if (state.products.length) {
          commit(MutationTypes.SET_VALUE, {
            key: "skip",
            value: state.products.length,
          });
        }
      }
      const { limit, skip, sort } = state;
      const params = { limit, skip, sort };
      const { data } = await axios.get<ProductResponse>(ApiRoutes.PRODUCT, {
        params,
      });
      const { isMoreAvaliable, products } = data;
      commit(MutationTypes.SET_VALUE, {
        key: "isMoreAvaliable",
        value: isMoreAvaliable,
      });
      if (skip) {
        for (const product of products) {
          commit(MutationTypes.ADD_PRODUCT, product);
        }
      } else {
        commit(MutationTypes.SET_VALUE, {
          key: "products",
          value: products,
        });
      }
      if (isMoreAvaliable) {
        commit(MutationTypes.SET_VALUE, {
          key: "skip",
          value: skip + limit,
        });
      }
    } catch (error) {
      console.log(error);
    }
  },
  async [ActionTypes.CREATE_PRODUCT]({ commit, state }) {
    try {
      const { data } = await axios.post(ApiRoutes.PRODUCT, state.product);
      if (data) {
        commit(MutationTypes.ADD_PRODUCT, state.product);
        commit(MutationTypes.SET_DEFAULT_PRODUCT);
      }
    } catch (error) {
      console.log(error);
    }
  },
  async [ActionTypes.UPDATE_PRODUCT]({ state, commit }) {
    try {
      console.log(state.product);
      void commit;

      const medias = state.product.medias.map((media: Media) => media._id);
      const payload = Object.assign({}, state.product, { medias });
      const { data } = await axios.put(ApiRoutes.PRODUCT, payload);
      if (data) {
        commit(MutationTypes.UPDATE_PRODUCT);
        commit(MutationTypes.SET_DEFAULT_PRODUCT);
      }
    } catch (error) {
      console.log(error);
    }
  },
  async [ActionTypes.DELETE_PRODUCT]({ commit }, id) {
    try {
      const { data } = await axios.delete(`${ApiRoutes.PRODUCT}/${id}`);
      if (data) {
        commit(MutationTypes.DELETE_PRODUCT, id);
      }
    } catch (error) {
      console.log(error);
    }
  },
  async [ActionTypes.UPLOAD_MEDIAS]({ state }, payload) {
    void state;
    const { data } = await axios.post(
      ApiRoutes.UPLOAD_MEDIAS,
      payload,
      uploadHeader
    );
    if (data) return data;
    else return null;
  },
};

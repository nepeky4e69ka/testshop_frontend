import { createStore, createLogger } from "vuex";
import { user, Store as UserStore } from "@/store/modules/user";
import { product, Store as ProductStore } from "@/store/modules/product";
import { State as UserState } from "./modules/user/state";
import { State as ProductState } from "./modules/product/state";

export type State = {
  user: UserState;
  product: ProductState;
};
export const store = createStore({
  plugins: process.env.NODE_ENV === "production" ? [] : [createLogger()],
  modules: { user, product },
});

export type Store = UserStore<Pick<State, "user">> &
  ProductStore<Pick<State, "product">>;

export function useStore(): Store {
  return store as Store;
}

export default store;

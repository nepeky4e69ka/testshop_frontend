import { Store as UserStore } from "./modules/user/index";

export interface RootState {
  version: string;
  user: UserStore;
}
export type SetValuePayload = {
  key: string;
  value: string | number | unknown;
};
export type SetSubValuePayload = {
  key: string;
  sub: string;
  value: string | number | unknown;
};

export interface Product {
  id?: unknown;
  _id?: string;
  name: string;
  price: number;
  medias: Media[];
}

export interface Media {
  _id: string;
  type: MediaType;
  filename: string;
}

export interface ProductResponse {
  products: Product[];
  isMoreAvaliable: boolean;
}

export enum MediaEnum {
  PHOTO = "PHOTO",
  VIDEO = "VIDEO",
  AUDIO = "AUDIO",
  DOCUMENT = "DOCUMENT",
  VOICE = "VOICE",
}
export type MediaType = "PHOTO" | "VIDEO" | "AUDIO" | "DOCUMENT" | "VOICE";

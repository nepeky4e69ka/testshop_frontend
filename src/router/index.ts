import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import { beforeEnter } from "../utils/helper";
import Signin from "../views/Signin.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "signin",
    component: Signin,
  },
  {
    path: "/products",
    name: "products",
    beforeEnter,
    component: () => import("../views/Products.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;

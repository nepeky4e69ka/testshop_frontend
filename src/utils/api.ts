export enum ApiRoutes {
  USER_SIGNIN = "auth/signin",
  USER_SIGNUP = "auth/signup",
  PRODUCT = "product",
  UPLOAD_MEDIAS = "media/upload",
}

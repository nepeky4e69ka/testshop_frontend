import axios, { AxiosResponse, AxiosError } from "axios";
import { removeCookie } from "./helper";
import { UserMutationTypes } from "../store/modules/user/mutations";
axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
axios.defaults.baseURL = process.env.VUE_APP_API_URL;
axios.interceptors.response.use(
  (config) => config,
  (error) => {
    if (error.response.status === 401) {
      removeCookie();
      destroyHttp();
      location.href = "/";
    } else {
      return Promise.reject(error);
    }
  }
);

const createHttp = (token: string): string =>
  (axios.defaults.headers.common["Authorization"] = `Bearer ${token}`);
const destroyHttp = (): string =>
  (axios.defaults.headers.common["Authorization"] = "");

const ErrorHandler = (error: AxiosError): void => {
  window.$store.commit(UserMutationTypes.SET_VALUE, {
    key: "error",
    value: error.response?.data.message,
  });
  setTimeout(() => {
    window.$store.commit(UserMutationTypes.SET_VALUE, {
      key: "error",
      value: null,
    });
  }, 4000);
};

export {
  axios,
  destroyHttp,
  AxiosResponse,
  createHttp,
  ErrorHandler,
  AxiosError,
};

import { App } from "vue";
import UIInput from "../components/input.vue";
import UIMedia from "../components/media.vue";
//
import ITrash from "../components/icons/trash.vue";
import ILock from "../components/icons/lock.vue";
import IUser from "../components/icons/user.vue";
import ISettings from "../components/icons/settings.vue";
export const components = (app: App): void => {
  app.component("ui-input", UIInput);
  app.component("ui-media", UIMedia);
  //
  app.component("i-trash", ITrash);
  app.component("i-lock", ILock);
  app.component("i-user", IUser);
  app.component("i-settings", ISettings);
};

import store from "@/store/index";
import { UserMutationTypes as MT } from "@/store/modules/user/mutations";
import { RouteLocationNormalized, NavigationGuardNext } from "vue-router";

export const beforeEnter = (
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext
): void => {
  if (store.state.user.isAuthenticated) {
    return next();
  }
  const token = getCookie();
  if (token) {
    store.commit(MT.SET_USER_AUTHENTICATED, { token });
    next();
  } else {
    next({ name: "signin" });
  }
};

export const processFileUpload = (
  input: HTMLInputElement
): Promise<FormData> => {
  return new Promise((resolve, reject) => {
    void reject;
    const input_files = input.files;
    if (input_files) {
      const formData = new FormData();
      for (const file of input_files) {
        formData.append("file", file);
      }
      resolve(formData);
    }
  });
};

export function jwtDecodeToUser(token: string): { email: string } | null {
  try {
    return JSON.parse(atob(token.split(".")[1]));
  } catch {
    return null;
  }
}

export const getCookie = (): string | null => {
  const name = "Authorization" + "=";
  let decodedCookie;
  try {
    decodedCookie = decodeURIComponent(document.cookie);
  } catch (e) {
    window.console.log({ decodeError: e });
    return "";
  }
  const ca = decodedCookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return null;
};

export const setCookie = (cvalue: string, exhour: number): void => {
  const d = new Date();
  d.setTime(d.getTime() + exhour * 60 * 60 * 1000);
  const expires = "expires=" + d.toUTCString();
  document.cookie = "Authorization" + "=" + cvalue + ";" + expires + ";path=/";
};

export const removeCookie = (): void => {
  const d = new Date();
  d.setTime(d.getTime() - 24 * 60 * 60 * 1000);
  const expires = "expires=" + d.toUTCString();
  document.cookie = "Authorization" + "=_;" + expires + ";path=/";
};

export const uploadHeader = {
  headers: { "Content-Type": "multipart/form-data" },
};

import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import moment from "moment";
import { components } from "./utils/components";
import "@/style/_index.scss";

const app = createApp(App);

components(app);
app.config.globalProperties.$moment = moment;
app.use(store).use(router).mount("#app");
window.$store = store;
